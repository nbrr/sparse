use std::fmt::Display;

use num_traits::identities::Zero;

pub mod csc;
pub mod dok;

pub trait Matrix<S: Display + Clone + Zero> {
    fn nrow(&self) -> usize;
    fn ncol(&self) -> usize;

    // fn set(&mut self, r: usize, c: usize);
    fn get(&self, r: usize, c: usize) -> S;

    fn to_string(&self) -> String {
        let mut str = String::new();

        for i in 0..self.nrow() {
            for j in 0..self.ncol() {
                str.push_str(&format!("{}", self.get(i, j)));
                if j != self.ncol() - 1 {
                    str.push(',');
                }
            }
            str.push('\n');
        }
        str
    }
}
