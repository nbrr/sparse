use std::fmt::Display;
use std::ops::{Add, Mul};

use num_traits::{One, Zero};

use crate::dok::DOK;
use crate::Matrix;

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct CSC<S: Display + Zero> {
    nrow: usize,
    ncol: usize,
    // TODO privatise this?
    pub val: Vec<S>,
    pub row_ind: Vec<usize>,
    pub col_ptr: Vec<usize>,
}

impl<S: Display + Clone + Zero> CSC<S> {
    pub fn new_uninitialized(r: usize, c: usize) -> Self {
        CSC {
            nrow: r,
            ncol: c,
            val: Vec::new(),
            row_ind: Vec::new(),
            col_ptr: vec![0],
        }
    }

    pub fn col_entries(&self, c: usize) -> Vec<(usize, S)> {
        assert!(c < self.ncol());
        let mut tmp = Vec::<(usize, S)>::new();
        for i in self.col_ptr[c]..self.col_ptr[c + 1] {
            tmp.push((self.row_ind[i], self.val[i].clone()));
        }

        tmp
    }
}

impl<S: Display + Clone + Zero> Matrix<S> for CSC<S> {
    fn get(&self, r: usize, c: usize) -> S {
        assert!(r < self.nrow);
        assert!(c < self.ncol);

        let col_start = self.col_ptr[c];
        let col_end = self.col_ptr[c + 1];

        let mut res = S::zero();

        for i in col_start..col_end {
            if self.row_ind[i] == r {
                res = self.val[i].clone();
                return res;
            }
        }

        res
    }

    fn nrow(&self) -> usize {
        self.nrow
    }

    fn ncol(&self) -> usize {
        self.ncol
    }
}

impl<S: Display + Clone + Zero> From<DOK<S>> for CSC<S> {
    fn from(dok: DOK<S>) -> CSC<S> {
        let mut res = CSC {
            nrow: dok.nrow(),
            ncol: dok.ncol(),
            val: Vec::new(),
            row_ind: Vec::new(),
            col_ptr: vec![0],
        };

        for j in 0..dok.ncol() {
            let mut non_zero = 0;
            for i in 0..dok.nrow() {
                let cell = dok.get(i, j).clone(); // FIXME clone
                if !cell.is_zero() {
                    res.val.push(cell);
                    res.row_ind.push(i);
                    non_zero += 1;
                }
            }
            res.col_ptr.push(res.col_ptr[j] + non_zero);
        }

        res
    }
}

impl<S: Display + Clone + Add + Mul + Zero + One> Mul<CSC<S>> for CSC<S> {
    type Output = CSC<S>;

    fn mul(self, rhs: CSC<S>) -> CSC<S> {
        assert!(self.ncol == rhs.nrow);
        let n = self.ncol;

        let mut res = CSC::new_uninitialized(self.nrow, rhs.ncol);
        let mut workspace = Vec::new();
        workspace.resize(n, S::zero());
        let mut nz = 0;

        // A*B = C
        // c_ij = S_k a_ik*b_kj
        // We work with columns -> fix j
        // then fix k
        // then browse the column with i
        for j in 0..res.ncol() {
            for (k, b) in rhs.col_entries(j) {
                for (i, a) in self.col_entries(k) {
                    workspace[i] = workspace[i].clone() + a.clone() * b.clone();
                }
            }

            for (i, val) in workspace.iter_mut().enumerate() {
                if !val.is_zero() {
                    nz += 1;
                    res.val.push(val.clone());
                    res.row_ind.push(i);
                    *val = S::zero();
                }
            }

            res.col_ptr.push(nz);
        }
        res
    }
}
impl<S: Display + Clone + Add + Mul + Zero + One> Mul<S> for CSC<S> {
    type Output = CSC<S>;

    fn mul(self, rhs: S) -> CSC<S> {
        let mut res = self.clone();
        for val in res.val.iter_mut() {
            *val = val.clone() * rhs.clone();
        }
        res
    }
}

#[cfg(test)]
mod test {
    use crate::csc::CSC;
    use crate::dok::DOK;

    #[test]
    fn col_entries() {
        let a: CSC<i64> =
            DOK::from_csv("1,2,3,4,5\n6,7,8,9,10\n11,12,13,14,15\n".to_string()).into();
        assert!(a.col_entries(1) == vec![(0, 2), (1, 7), (2, 12)]);
    }

    #[test]
    fn mul() {
        let i5: DOK<i64> = DOK::identity(5);
        let i5_csc = CSC::from(i5);
        let i5_csc1 = i5_csc.clone();
        let i5_csc2 = i5_csc.clone();

        assert!(i5_csc1 * i5_csc2 == i5_csc);

        let a: CSC<i64> = DOK::from_csv("1,1,1,1,1\n1,1,1,1,1\n1,1,1,1,1\n".to_string()).into();
        let b: CSC<i64> =
            DOK::from_csv("1,1,1,1\n1,1,1,1\n1,1,1,1\n1,1,1,1\n1,1,1,1\n".to_string()).into();

        let c: CSC<i64> = DOK::from_csv("5,5,5,5\n5,5,5,5\n5,5,5,5".to_string()).into();

        assert!(a * b == c);
    }
}
