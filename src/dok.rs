use std::collections::HashMap;
use std::fmt::Display;
use std::ops::{Add, Mul};

use num_traits::{One, Zero};

use data_reader::reader::*;

use crate::csc::CSC;
use crate::Matrix;

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct DOK<S: Display + Zero> {
    nrow: usize,
    ncol: usize,
    data: HashMap<(usize, usize), S>,
}

impl<S: Display + Zero> DOK<S> {
    pub fn new(nrow: usize, ncol: usize) -> Self {
        DOK {
            nrow,
            ncol,
            data: HashMap::new(),
        }
    }

    pub fn set(&mut self, r: usize, c: usize, v: S) {
        assert!(r < self.nrow);
        assert!(c < self.ncol);
        self.data.insert((r, c), v);
    }
}

impl<S: Display + Zero + One> DOK<S> {
    pub fn identity(s: usize) -> Self {
        let mut m = Self::new(s, s);
        for i in 0..s {
            m.set(i, i, One::one());
        }
        m
    }
}

impl<S: Display + Clone + Zero> Matrix<S> for DOK<S> {
    fn nrow(&self) -> usize {
        self.nrow
    }

    fn ncol(&self) -> usize {
        self.ncol
    }

    fn get(&self, r: usize, c: usize) -> S {
        if let Some(value) = self.data.get(&(r, c)) {
            value.clone() // FIXME clone?
        } else {
            S::zero()
        }
    }
}

impl<S: Display + Clone + Zero> Add<DOK<S>> for DOK<S> {
    type Output = DOK<S>;

    // TODO be efficient
    fn add(self, rhs: DOK<S>) -> DOK<S> {
        let nrow = self.nrow;
        let ncol = self.ncol;

        assert!(nrow == rhs.nrow);
        assert!(ncol == rhs.ncol);

        let mut res = DOK::new(nrow, ncol);

        for i in 0..nrow {
            for j in 0..ncol {
                let cell = self.get(i, j).clone() + rhs.get(i, j).clone(); // FIXME ref
                if !cell.is_zero() {
                    res.set(i, j, cell);
                }
            }
        }

        res
    }
}

impl<S: Display + Clone + Add + Zero + One> Mul<DOK<S>> for DOK<S> {
    type Output = DOK<S>;

    // TODO be efficient
    fn mul(self, rhs: DOK<S>) -> DOK<S> {
        assert!(self.ncol == rhs.nrow);
        let n = self.ncol;

        let mut res = DOK::new(self.nrow, rhs.ncol);
        for i in 0..self.ncol {
            for j in 0..rhs.nrow {
                let mut cell = S::zero();
                for k in 0..n {
                    cell = cell + self.get(i, k).clone() * rhs.get(k, j).clone(); // FIXME clone
                }
                if !cell.is_zero() {
                    res.set(i, j, cell);
                }
            }
        }
        res
    }
}

impl<S: Display + Clone + Zero> From<CSC<S>> for DOK<S> {
    fn from(csc: CSC<S>) -> DOK<S> {
        let mut res = DOK {
            nrow: csc.nrow(),
            ncol: csc.ncol(),
            data: HashMap::new(),
        };

        for j in 0..csc.ncol() {
            for k in csc.col_ptr[j]..csc.col_ptr[j + 1] {
                res.set(csc.row_ind[k], j, csc.val[k].clone())
            }
        }

        res
    }
}

impl DOK<i64> {
    pub fn from_csv(csv: String) -> DOK<i64> {
        let nrow = csv.lines().count();
        let ncol = csv.lines().next().unwrap().split(',').count(); // FIXME gross

        let mut res = DOK::new(nrow, ncol);

        let mut lines = csv.lines();
        for i in 0..nrow {
            let mut numbers = lines.next().unwrap().split(',');
            for j in 0..ncol {
                if let Some(c) = numbers.next() {
                    res.set(i, j, c.parse::<i64>().unwrap());
                }
            }
        }

        res
    }

    pub fn from_csv_file(file: &str) -> DOK<i64> {
        let params = ReaderParams {
            comments: b'%',
            delimiter: Delimiter::Any(b','),
            skip_header: None,
            skip_footer: None,
            usecols: None,
            max_rows: None,
        };

        let input = load_txt_i64(file, &params).unwrap();
        let (m, n) = (input.num_lines, input.num_fields);

        let mut res = DOK::new(m, n);

        for i in 0..m {
            for j in 0..n {
                res.set(i, j, input.results[n * i + j]);
            }
        }

        res
    }
}

#[cfg(test)]
mod test {
    use crate::dok::DOK;
    use crate::Matrix;

    #[test]
    fn from_str() {
        let str = "0,12,0,0,32,0,0,0,50,9,0,8,0,0,0,0,7
5,0,0,0,0,0,0,0,6,0,0,8,7,9,0,0,0
20,5,0,0,0,6,8,5,0,0,0,0,6,0,0,6,0
0,0,0,6,4,84,45,0,0,152,0,51,0,0,0,6,0
0,8,0,0,7,10,0,7,210,0,0,8,0,7,9,69,351
60,98,1,0,0,105,0,0,60,60,0,70,0,0,0,0,0
0,0,0,80,0,0,40,0,6,0,0,7,0,2,615,165,3
"
        .to_string();

        let m = DOK::from_csv(str.clone());

        assert_eq!(str, m.to_string());
    }
}
