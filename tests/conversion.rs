use rand::Rng;

use sparse::csc::CSC;
use sparse::dok::DOK;

#[test]
fn dok_csc() {
    let dok = random_matrix(100, 100);
    let csc_from_dok = CSC::from(dok.clone());
    let dok_from_csc_from_dok = DOK::from(csc_from_dok.clone());
    assert_eq!(dok, dok_from_csc_from_dok);
}

fn random_matrix(r: usize, c: usize) -> DOK<i64> {
    let mut res = DOK::new(r, c);

    let mut rng = rand::thread_rng();
    for i in 0..r {
        for j in 0..c {
            let val = rng.gen();
            res.set(i, j, val)
        }
    }

    res
}
